# codemirror-demo
Demo project to test out codemirror and webpack setup.

## Getting started

```
# to install dependencies
yarn install

# run dev server with hot module replacement
yarn dev

# run bundler to create production build (in dist directory)
yarn build

```
